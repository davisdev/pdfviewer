<?php

// Redirect default route to default API endpoint.
$router->get('/', function() {
    return redirect(route('documents.index'));
});

$router->group(['prefix' => 'api/documents'], function () use ($router) {
    $router->get('/', ['uses' => 'DocumentController@index', 'as' => 'documents.index']);
    $router->post('/', ['uses' => 'DocumentController@store', 'as' => 'documents.store']);
    $router->get('{document}', ['uses' => 'DocumentController@show', 'as' => 'documents.show']);
    $router->delete('{document}', ['uses' => 'DocumentController@delete', 'as' => 'documents.delete']);
});
