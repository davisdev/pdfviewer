<?php

namespace App\Http\Controllers;

use App\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class DocumentController extends Controller
{
    /**
     * @var array
     */
    protected $validationMessages = [
        'document.required' => 'Document must be provided!',
        'document.mimes'    => 'Only PDF documents allowed!',
        'document.max'      => 'Max file size of :max exceeded!'
    ];

    /**
     * Get all the documents.
     *
     * @return Document[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Document::all()->filter(function($document) {
            return Storage::exists('/documents/' . $document->filename . '.pdf');
        });
    }

    /**
     * Retrieve specific document.
     *
     * @param $filename
     * @return BinaryFileResponse
     */
    public function show($filename)
    {
        // Correct headers for successful encoding.
        $headers = ['Content-Type' => 'application/pdf'];

        if (Storage::exists('/documents/' . $filename . '.pdf')) {
            // Blob response for Javascript to render.
            return new BinaryFileResponse(
                storage_path() . '/app/documents/' . $filename . '.pdf',
                200,
                $headers
            );
        }

        return response()->json([
            'error'  => 'File does not exist!'
        ], 404);
    }

    /**
     * Create new document model and store
     * the pdf in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        // Making validator from factory to easily access failed state.
        $validator = $this->getValidationFactory()->make($request->all(), [
            'pdf' => 'required|file|mimes:pdf|max:5012'
        ], $this->validationMessages);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->getMessageBag()
            ], 422);
        }

        // Create file name for storage.
        $uploadedFile = $request->file('pdf');
        $fileName = $uploadedFile->getClientOriginalName();
        $uniqueFileName = $this->createUniqueName($fileName);


        $storedFile = $uploadedFile->storeAs('documents', $uniqueFileName);

        // When file is stored successfully, create model for it.
        if ($storedFile) {
            Document::create([
                // Success on storing file returns stored file path as string.
                'title'    => $fileName,
                'filename' => str_ireplace('.pdf', '', $uniqueFileName)
            ]);
        }

        return response()->json([
            'success' => 'Document has been uploaded!'
        ], 200);
    }

    /**
     * Create unique store-able file name.
     *
     * @param $document
     * @return string
     */
    private function createUniqueName($document)
    {
        $beginning = str_ireplace('.pdf', '', $document);

        return str_slug($beginning . '_' . uniqid()) . '.pdf';
    }
}