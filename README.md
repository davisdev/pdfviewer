## Preface

The essence of the project is withing .env file which you will have to rename from .env.example
to .env, so make sure to do it before running `docker-compose up`.

.env files has configuration that will be applied to container names, database settings etc.

## Installation

1. Rename `.env.example` to `.env`
2. Make sure to set `PROJECT`, `MYSQL_DATABASE`, `MYSQL_USER`, `MYSQL_ROOT_PASSWORD`.
Other environment variables will be set already in example file
3. MySQL database settings **must** match with Lumen related database settings for migrations
4. Open project root and run the docker-compose: `docker-compose up`
5. SSH into _backend_ service & run migrations, - all together with `docker-compose exec backend php artisan migrate` (subsequent migrations need `:refresh` after `migrate` to avoid duplicates in database)
6. Navigate to `http://localhost:8000` and project should be running


**Tip:**
You can add desired hostname to your `/etc/hosts` file as `127.0.0.1 <hostname>` to be able to access frontend as `http://<hostname>:8000`

## Additional things
You can browse to `http://localhost:8080` to log into _adminer_ and observe database.