const EventBus = new Vue

const maxFileSize = 5242880; // 5 Mb

/**
 * Check if file size is OK for upload.
 * 
 * @param {File} file 
 */
function isValidSize(file) {
  return file.size <= maxFileSize
}

// Refining axios instance to have baseurl (for reusability).
// And for the sake of giving it a better name.
const request = axios.create({
  baseURL: 'http://localhost:9000/api/documents' // How neat, one URL, so many HTTP methods...
})

Vue.component("pdf", {
  template: `
        <div class="col-md-3" @click="openDocument(pdf)">
            <div class="card">
                <img src="//placehold.it/200x150" alt="PDF" class="card-img-top" />
                <div class="card-body">
                    <p class="card-text" v-text="pdf.filename"></p>
                </div>
            </div>
        </div>
    `,

  props: ["pdf"],

  data() {
    return {
      selected: null
    };
  },

  methods: {
    /**
     * Prepare document for display.
     * 
     * @param {*} document 
     */
    async openDocument(document) {
      this.selected = document
      let documentBlob = await this.requestPdf(this.selected)

      // If the request succeeds, show the modal.
      // Can't check properly since promise will be
      // rejected only when there's network error.
      if (documentBlob) {
        this.launchModal(documentBlob)
      }
    },

    /**
     * Retrieve specified file from storage.
     * 
     * @param {*} pdf 
     */
    requestPdf(pdf) {
      return request.get('/' + pdf.filename, {
        responseType: 'arraybuffer',
        headers: {
          'Accept': 'application/pdf',
          'Content-Type': 'application/pdf'
        }
      }).then(response => {
        return URL.createObjectURL(new Blob([response.data], {
          type: 'application/pdf'
        }))
      }).catch(error => {
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'PDF not found!'
        })
      })
    },

    launchModal(currentDocument) {
      EventBus.$emit('launchModal', currentDocument)
    }
  }
});

Vue.component("pdf-grid", {
  template: `
        <div>
            <div class="alert alert-info text-center" v-if="!this.documents">
                No PDF documents available.
            </div>
            <div class="row" v-else>
              <pdf v-for="document in this.documents" :key="document.id" :data-pdf="document.filename" :pdf="document"></pdf>
            </div>
        </div>
    `,

  data() {
    return {
      documents: []
    };
  },

  mounted() {
    /**
     * Retrieving currently stored PDF's.
     */
    request.get("/")
      .then(response => this.documents = response.data)
      .catch(error => console.log(error.message));
  }
});

Vue.component("upload", {
  template: `
        <form method="post" action="#" class="form-horizontal" @submit.prevent="handleUpload(this.pdfForm)">
            <div class="form-group">
                <label for="file"><slot name="label">Upload your own file: </slot></label><br>
                <input type="file" id="file" name="pdf" ref="file" @input="assignPdf()" accept="application/pdf">
                <button class="btn btn-info" type="submit" :disabled="!this.file || this.invalid">Upload</button>
            </div>
            <div class="alert alert-danger" v-if="this.invalid">Max allowed file size exceeded!</div>
        </form>
    `,

  data() {
    return {
      file: null,
      invalid: false
    };
  },

  methods: {
    /**
     * Assign currently existent file in form.
     * 
     * @param {Event} event 
     */
    assignPdf() {
      this.file = null
      let file = this.$refs.file.files[0]

      if (isValidSize(file)) {
        this.invalid = false
        this.file = this.$refs.file.files[0];

        return
      }

      this.invalid = true
    },

    /**
     * Upload PDF to file storage.
     */
    handleUpload() {
      let formData = new FormData();
      formData.append("pdf", this.file);

      request.post('/', formData, {
        headers: { 'Content-Type': 'multipart/form-data' }
      }).then(response => {
        Swal.fire({
          type: 'success',
          title: 'Success!',
          text: response.message
        }).then(() => location.reload())

        // Reset form and re-render array (by default arrays are not reactive)
        this.file = this.$refs.file.value = null

        return
      })
        .catch(error => {
          return Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: error.message
          })
        })
    }
  }
});

Vue.component('modal', {
  template: `
    <div class="modal" id="pdf-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-full" role="document"> 
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <iframe :src="this.content" type="application/pdf" width="100%" height="100%" frameborder="0" scrolling="no"></iframe>
          </div>
        </div>
      </div>
    </div>
  `,

  props: [
    'title',
    'close-title'
  ],

  data() {
    return {
      content: null,
      visible: false
    }
  },

  mounted() {
    EventBus.$on('launchModal', document => {
      this.triggerVisible(document)
    })

    EventBus.$on('closeModal', event => {
      this.content = null
    })
  },

  methods: {
    /**
     * Set Blob content and show modal.
     * 
     * @param {Blob} source 
     */
    triggerVisible(source) {
      this.content = source

      $('#pdf-modal').modal({
        show: true
      });
    },

    /**
     * Reset Blob content and hide modal.
     */
    triggerClosed() {
      $('#pdf-modal').modal({
        show: false
      })

      this.content = null
    }
  }
})

// Binding Vue root instance.
new Vue({
  el: ".container"
});